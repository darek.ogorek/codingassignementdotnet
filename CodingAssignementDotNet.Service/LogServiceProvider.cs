﻿using CodingAssignementDotNet.DB;
using CodingAssignementDotNet.DB.Models;
using CodingAssignementDotNet.LogService.Interface;
using CodingAssignementDotNet.LogService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CodingAssignementDotNet.LogService
{
    public class LogServiceProvider : ILogServiceProvider
    {
        private readonly ILogReader _logReader;
        private readonly IDatabaseService _databaseService;
        public LogServiceProvider(ILogReader logReader, IDatabaseService databaseService )
        {
            _logReader = logReader;
            _databaseService = databaseService;
            Initializer();
        }

        private void Initializer()
        {
            _databaseService.SetEnvironment(@"MyOwnDatabase");
        }

        public void CalculateEventCase(FileData start, FileData end)
        {
            _databaseService.Insert<Event>(
                new Event()
                {
                    Alert = (end.TimeSpan.Millisecond - start.TimeSpan.Millisecond) >= 4,
                    Duration = (end.TimeSpan.Millisecond - start.TimeSpan.Millisecond).ToString(),
                    Host= start.Host,
                    ID = start.Id,
                    Type= start.Type
                });

        }

        public async Task Evaluate(string path)
        {
            new Task(() =>
            {
                _logReader.ReadFile(path);
            }).Start();

            new Task(() =>
            {
                Task.Delay(1000).Wait();
                FileData startedFileData = _logReader.GetStartedFileData();
                while (startedFileData != null)
                {
                    System.Console.WriteLine("Started "+ startedFileData.Id);
                    FileData finishedFileData = _logReader.GetFinishedFileData(startedFileData.Id);
                   // System.Console.WriteLine(finishedFileData.Id);

                    if (startedFileData != null && finishedFileData != null)
                    {
                        System.Console.WriteLine("Finished " + finishedFileData.Id);
                        System.Console.WriteLine("Elapsed Time" + (finishedFileData.TimeSpan.Millisecond - startedFileData.TimeSpan.Millisecond));
                        CalculateEventCase(startedFileData, finishedFileData);

                        startedFileData = _logReader.GetStartedFileData();
                    }
                }
            }).Start();

            //Task.Run(() =>
            //{
            //    _logReader.ReadFile(path);
            //}).Start();
        }

        public bool Delete()
        {
            return _databaseService.Delete();
        }
    }
}
