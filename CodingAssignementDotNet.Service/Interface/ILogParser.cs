﻿using CodingAssignementDotNet.LogService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingAssignementDotNet.LogService.Interface
{
    public interface ILogParser
    {
     //   Task<FileData> Parse(string input);
        FileData Parse(string input);
    }
}
