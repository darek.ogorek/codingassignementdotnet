﻿using CodingAssignementDotNet.DB.Models;
using CodingAssignementDotNet.LogService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingAssignementDotNet.LogService.Interface
{
    public interface ILogServiceProvider
    {
        Task Evaluate(string path);
        void CalculateEventCase(FileData start, FileData end);
        bool Delete();

    }
}
