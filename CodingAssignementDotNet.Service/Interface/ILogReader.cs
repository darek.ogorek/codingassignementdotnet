﻿using CodingAssignementDotNet.LogService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingAssignementDotNet.LogService.Interface
{
    public interface ILogReader
    {
        bool ReadFile(string filePath);
        FileData GetStartedFileData();
        FileData GetFinishedFileData(string id);

    }
}
