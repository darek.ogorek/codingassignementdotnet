﻿using CodingAssignementDotNet.LogService.Interface;
using CodingAssignementDotNet.LogService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CodingAssignementDotNet.LogService.Service
{
    public class LogParser : ILogParser
    {
        public FileData  Parse(string input)
        {
           return  ParseRawData(GetRawData(input));
        }

        private RawFileData GetRawData(string input)
        {
            RawFileData newData = new RawFileData();
            string[] splittedInput = input.Trim('{').Trim('}').Split(new char[] { ',' });
            foreach (String part in splittedInput)
            {
                string[] partSplitted = part.Split(new char[] { ':' });
                if (partSplitted.Count() != 2) continue;
                Type type = newData.GetType();
                PropertyInfo info = type.GetProperty(partSplitted[0].Trim(' ').TrimStart('"').TrimEnd('"'));
                if (info == null) { return null; }
                info.SetValue(newData, partSplitted[1].Trim('"'));
            }
            return newData;
        }
        private FileData ParseRawData(RawFileData input)
        {
            return new FileData
            {
                Host = input.host,
                Id = input.id,
                State = (State)Enum.Parse(typeof(State), input.state),
                Type = input.type,
                TimeSpan = TimeStampToDateTime(input.timestamp)
            };                
        }
        private  DateTime TimeStampToDateTime(string timeStamp)
        {
            // Java timestamp is milliseconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(Convert.ToDouble(timeStamp)).ToLocalTime();
            return dtDateTime;
        }

    }
}
