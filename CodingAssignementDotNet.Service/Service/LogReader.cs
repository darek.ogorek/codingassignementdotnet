﻿using CodingAssignementDotNet.LogService.Interface;
using CodingAssignementDotNet.LogService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace CodingAssignementDotNet.LogService.Service
{
    public class LogReader : ILogReader
    {
        private ConcurrentStack<FileData> _listOfStartedData;
        private ConcurrentDictionary<string, FileData> _listFinishedData = new ConcurrentDictionary<string, FileData>();

        private readonly ILogParser _logParser;
        public LogReader(ILogParser parser)
        {
            _logParser = parser;
            _listOfStartedData = new ConcurrentStack<FileData>();
        }

        public bool ReadFile(string filePath)
        {
            string line;
            using (System.IO.StreamReader file = new System.IO.StreamReader(filePath))
            {
                while ((line = file.ReadLine()) != null)
                {
                    FileData data = _logParser.Parse(line);
                    if (data.State == State.STARTED)
                        _listOfStartedData.Push(data);
                    if (data.State == State.FINISHED)
                        _listFinishedData.TryAdd(data.Id, data);
                }
            }
            return true;
            //     new Thread(() =>
            //     {
            //         for (int i = 0; i < 5; i++)
            //         {
            //             Thread.Sleep(200); // do work
            //             list.Add(i);
            //         }
            //         tcs.SetResult(true);
            //         Thread.Sleep(2000); //do more work
            //         Console.WriteLine("The End -- Press Enter");
            //         Console.ReadLine();
            //     }
            //).Start();

            //     //This task awaits for the list to be populated
            //     new Task(async () =>
            //     {
            //         await Task.Delay(300); //do work
            //         await tcs.Task;
            //         Console.WriteLine("List count = " + list.Count);
            //     }).Start();

            //while (!cb.IsEmpty)
            //{
            //    bagConsumeTasks.Add(Task.Run(() =>
            //    {
            //        int item;
            //        if (cb.TryTake(out item))
            //        {
            //            Console.WriteLine(item);
            //            itemsInBag++;
            //        }
            //    }));
            //}
            //Task.WaitAll(bagConsumeTasks.ToArray());

        }

        public FileData GetStartedFileData()
        {
            FileData data;
            if (_listOfStartedData.TryPop(out data))
                return data;
            return null;
        }

        public FileData GetFinishedFileData(string id)
        {
            FileData data;
            if (_listFinishedData.TryGetValue(id, out data))
                return data;
            return null;
        }
    }
}
