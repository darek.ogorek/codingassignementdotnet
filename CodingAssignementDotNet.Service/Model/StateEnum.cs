﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingAssignementDotNet.LogService.Model
{
    public enum State
    {
        STARTED, FINISHED
    }
}
