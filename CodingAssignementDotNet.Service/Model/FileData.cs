﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingAssignementDotNet.LogService.Model
{
    public class FileData
    {
        public string Id { get; set; } 
        public State State { get; set; }  //State
        public string Type { get; set; }
        public string Host { get; set; }
        public DateTime TimeSpan { get; set; } //TimeSpan
     }
}
