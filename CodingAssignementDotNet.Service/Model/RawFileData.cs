﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingAssignementDotNet.LogService.Model
{
   public class RawFileData
    {
        public string id { get; set; }
        public string state { get; set; }  //State
        public string type { get; set; }
        public string host { get; set; }
        public string timestamp { get; set; } //TimeSpan
    }
}
