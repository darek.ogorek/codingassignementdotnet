﻿using CodingAssignementDotNet.DB.Models;
using CodingAssignementDotNet.LogService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingAssignementDotNet.DB
{
    public interface IDatabaseService
    {
        bool Open();
        void SetEnvironment(string path);
        bool Insert(Event data);
        bool Insert<T>(T data) where T : class, IRepositoryModel;
        bool Insert<T>(ICollection<T> data) where T : class, IRepositoryModel;

        bool Delete();

    }
}
