﻿using CodingAssignementDotNet.DB.Models;
using CodingAssignementDotNet.LogService.Interface;
using LiteDB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingAssignementDotNet.DB
{
    /// <summary>
    /// Class for basic database operation
    /// </summary>
    public  class DatabaseService : IDatabaseService
    {
        private string _path;
        public void SetEnvironment(string path)
        {
            _path = path;
        }
        public bool Open()
        {
            using (var db = new LiteRepository(_path))
            {
            }
            return true;
        }
        /// <summary>
        /// To Remove if not needed
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool Insert(Event data)
        { 
            using (var db = new LiteRepository(_path))
            {
                db.Insert(data);
            }
            return true;
        }

        /// <summary>
        /// Generic method for inserting data 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool Insert<T>(T data)
            where T : class, IRepositoryModel
        {
            using (var db = new LiteRepository(_path))
            {
                db.Insert<T>(data);            
            }
            return true;
        }
 

       public bool Insert<T>(ICollection<T> data)
            where T : class, IRepositoryModel
        {
            using (var db = new LiteRepository(_path))
            {
                db.Insert<T>(data);
            }
            return true;
        }

        public bool Delete()
        {
            try {

                if (File.Exists(_path))
                {
                // If file found, delete it    
                    File.Delete(_path);
                    Console.WriteLine("File deleted.");
                }
                else Console.WriteLine("File not found");
            }    
            catch (IOException ioExp)    
            {    
                Console.WriteLine(ioExp.Message);
                return false;
            }
            return true;
        }
    }
}
