﻿using CodingAssignementDotNet.LogService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingAssignementDotNet.DB.Models
{
   public  class Event: IRepositoryModel
    {
        public string ID { get; set; }
        public string Duration { get; set; }
        public string Type { get; set; }
        public string Host { get; set; }
        public bool Alert { get; set; }
    }
}
