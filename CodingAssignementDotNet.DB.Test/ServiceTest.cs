﻿using System;
using CodingAssignementDotNet.DB.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CodingAssignementDotNet.DB.Test
{
    [TestClass]
    public class ServiceTest
    {
        private string _dbPath;
        IDatabaseService _db;
  

        [TestInitialize]
        public void TestInitialize()
        {
            _dbPath = @"TestBD";
            _db = new DatabaseService();
            _db.SetEnvironment(_dbPath);
        }

        [TestMethod]
        public void Open_ShouldCreateNewDB()
        { 
           bool isCreated = _db.Open();
           Assert.AreEqual(isCreated, true);
        }

        [TestMethod]
        public void Insert_Should_InsertData()
        {
            Event eventData = new Event()
            {
                Alert = true,
                Duration = "5431",
                ID = "someID1",
                Type = "type1",
                Host = ""
            };
             
          //  _db.Open(_dbPath);
            bool isCreated = _db.Insert<Event>(eventData);
            Assert.AreEqual(isCreated, true);        
        }



        [TestCleanup]
        public void CleanUp()
        {
            System.IO.File.Delete(_dbPath);

        }
    }
}
