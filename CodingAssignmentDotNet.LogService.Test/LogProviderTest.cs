﻿using System;
using CodingAssignementDotNet.LogService.Interface;
using CodingAssignementDotNet.LogService.Model;
using CodingAssignementDotNet.LogService.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CodingAssignmentDotNet.LogService.Test
{
    [TestClass]
    public class LogProviderTest
    {
        private  ILogReader _logProvider;
        private  ILogParser _logParser;
        private string _fileData;

        [TestInitialize]
        public void Initialize()
        {
            _logParser = new LogParser();
            _logProvider = new LogReader(_logParser);
            _fileData = @"\TestData\TestData1.txt";
        }

        [TestMethod]
        public void ReadFile_ShouldReturns_True()
        {
           bool isLoaded = _logProvider.ReadFile(_fileData);
            Assert.IsTrue(isLoaded);

        }

        [TestMethod]
        public void PopFileData_ShouldReturns_True()
        {
            bool isLoaded = _logProvider.ReadFile(_fileData);
            Assert.IsTrue(isLoaded);

            FileData data = _logProvider.GetStartedFileData();
            Assert.IsNotNull(data);

        }
    }
}
