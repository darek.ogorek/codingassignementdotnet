﻿using System;
using CodingAssignementDotNet.LogService.Interface;
using CodingAssignementDotNet.LogService.Model;
using CodingAssignementDotNet.LogService.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace CodingAssignmentDotNet.LogService.Test
{
 
    [TestClass]
    public class LogParserTest
    { 
        private ILogParser _logParser;
        private string _fileData;

        [TestInitialize]
        public void Initialize()
        {
            _logParser = new LogParser();  
        }

        [TestMethod]
        public void Parser_ShouldReturns_FinishedFileData()
        {
            string data = "{ \"id\":\"scsmbstgrb\", \"state\":\"FINISHED\", \"timestamp\":1491377495216}";
            FileData  parsedData= _logParser.Parse(data);
            Assert.AreEqual(parsedData.Host, null);
            Assert.AreEqual(parsedData.Id, "scsmbstgrb");
            Assert.AreEqual(parsedData.State, State.FINISHED);
            //Assert.AreEqual(parsedData.TimeSpan, DateTime.pa);
            Assert.AreEqual(parsedData.Type, null);

        }
        [TestMethod]
        public void Parser_ShouldReturns_FileDataWithAllProperties()
        {
            string data = "{\"id\":\"scsmbstgra\", \"state\":\"FINISHED\", \"type\":\"APPLICATION_LOG\", \"host\":\"12345\", \"timestamp\":1491377495217}";
            FileData parsedData = _logParser.Parse(data);
            Assert.AreEqual(parsedData.Host, "12345");
            Assert.AreEqual(parsedData.Id, "scsmbstgra");
            Assert.AreEqual(parsedData.State, State.FINISHED);
            Assert.IsNotNull(parsedData.TimeSpan);
            Assert.AreEqual(parsedData.Type, "APPLICATION_LOG");

        }
    }
}

