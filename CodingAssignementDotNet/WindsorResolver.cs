﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using CodingAssignementDotNet.DB;
using CodingAssignementDotNet.LogService;
using CodingAssignementDotNet.LogService.Interface;
using CodingAssignementDotNet.LogService.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingAssignementDotNet
{
    public class WindsorResolver : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            //container.Register(Classes.FromThisAssembly()
            //           .BasedOn<IController>()
            //         .LifestyleScoped<HybridPerWebRequestLifetimeScopeAccessor>());

            //container.AddFacility<TypedFactoryFacility>();


            container.Register(Component.For<ITest>()
                 .ImplementedBy<TestClass>());

            container.Register(Component.For<ILogReader>()
                 .ImplementedBy<LogReader>().LifestylePerThread());
            container.Register(Component.For<ILogParser>()
                 .ImplementedBy<LogParser>().LifestylePerThread());
            container.Register(Component.For<ILogServiceProvider>()
                 .ImplementedBy<LogServiceProvider>().LifestylePerThread());
            container.Register(Component.For<IDatabaseService>()
                 .ImplementedBy<DatabaseService>().LifestylePerThread());

            


            //container.Register(Component.For<ICommandDispatcherService>().ImplementedBy<CommandDispatcherService>()
            // .LifeStyle.HybridPerWebRequestLifetime());

            //container.Register(Classes.FromAssembly(typeof(ICommandHandler).Assembly)
            //    .BasedOn(typeof(CommandHandler<>))
            //    .If(p => !p.IsInterface)
            //    .WithServiceBase().LifestyleScoped<HybridPerWebRequestLifetimeScopeAccessor>());


            container.Register(Component.For<IWindsorContainer>().Instance(container));
        }
    }
}