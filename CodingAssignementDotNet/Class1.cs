﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingAssignementDotNet
{
    public interface ITest
    {
        bool TestMethod();
    }
   public class TestClass: ITest
    {
        public bool TestMethod()
        {
            return true;
        }
    }
}
