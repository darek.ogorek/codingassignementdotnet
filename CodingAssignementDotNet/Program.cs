﻿using Castle.Windsor;
using Castle.Windsor.Installer;
using CodingAssignementDotNet.LogService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingAssignementDotNet
{
    class Program
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetLogger("logfile");//.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            MainAsync(args).GetAwaiter().GetResult();
        }

        static async Task MainAsync(string[] args)
        {

            IWindsorContainer _container = new WindsorContainer().Install(new WindsorResolver());
            var logServiceProvider = _container.Resolve<ILogServiceProvider>();
            logServiceProvider.Delete();
            Console.WriteLine(args[0]);
             logServiceProvider.Evaluate(args[0]).GetAwaiter().GetResult();// @"C:\Users\Wieior\source\repos\CodingAssignementDotNet\CodingAssignmentDotNet.LogService.Test\TestData\TestData1.txt");

             Console.ReadLine();
             NLog.LogManager.Shutdown();
        }
      
    }
}
